#!/usr/bin/python3

import logging

import numpy as np
import scipy.sparse as sp


logger = logging.getLogger(__name__)

def generate(g, config):
    separate_literals = config['graph']['structural']['separate_literals']
    # create mapping to integers [0, ...]
    edges_dict = {edge: i for i, edge in enumerate(g.edges(unique=True))}
    nodes_dict = {node: i for i, node in enumerate(g.nodes(separate_literals))}
    num_nodes = len(nodes_dict)

    # generate symmetrically normalized adjacency matrix 
    A = generate_adjacency_matrix(g,
                                  nodes_dict,
                                  num_nodes,
                                  separate_literals,
                                  config['graph']['structural'])

    return [A, nodes_dict, edges_dict]

def generate_adjacency_matrix(g,
                              nodes_dict,
                              num_nodes,
                              separate_literals,
                              config):
    undirected_graph = not config['directed_graph']
    shape = (num_nodes, num_nodes)
    logger.debug("Generating {} adjacency matrix".format(shape))

    coordinates = set()
    for h, r, t in g.triples(separate_literals=separate_literals):
        h_idx = nodes_dict[h]
        t_idx = nodes_dict[t]

        coordinates.add((h_idx, t_idx))
        if undirected_graph:
            # also add reverse links
            coordinates.add((t_idx, h_idx))

        # add self-loop
        coordinates.add((h_idx, h_idx))
        coordinates.add((t_idx, t_idx))

    # adjacency matrix
    data = np.ones(len(coordinates), dtype=np.int8)
    row, col = list(zip(*coordinates))
    A = sp.csr_matrix((data, (row, col)), shape=shape, dtype=np.int8)

    # inversely squared degree matrix
    data = np.power(np.array(A.sum(1).flatten()).squeeze(), -0.5)
    assert max(data.max()) < np.power(2, 16)//2  # ensure that int16 is enough
    D_inv_sqrt = sp.diags(data, shape=shape, dtype=np.int16)

    # normalize symmetrically
    A = D_inv_sqrt.dot(A).dot(D_inv_sqrt)

    return A
