#!/usr/bin/env python

import torch
import torch.nn as nn


class GraphConvolutionLayer(nn.Module):
    def __init__(self, input_dim, output_dim, num_nodes, bias=False,
                 input_layer=False, featureless=False):
        """
        Graph Convolutional Layer
        """
        super().__init__()

        self.input_dim = input_dim  # no. of input features
        self.output_dim = output_dim  # no. of output features
        self.num_nodes = num_nodes  # no. of nodes (batch size)
        self.input_layer = input_layer
        self.featureless = featureless
        self.bias = bias

        self.W_I = None
        self.W_F = None
        self.b = None

        # weights for identities and features
        if self.input_layer:
            self.W_I = nn.Parameter(torch.empty((self.num_nodes, self.output_dim)))
        if not self.featureless:
            self.W_F = nn.Parameter(torch.empty((self.input_dim, self.output_dim)))

        # declare bias
        if self.bias:
            self.b = nn.Parameter(torch.empty(self.output_dim))

        # initialize weights
        self.init()

    def forward(self, X, A):
        # if input layer: AXW = A[I F]W = AIW_I + AFW_F
        # else:           AXW = AHW

        AIW_I = 0.0
        if self.input_layer:
            # AIW_I = AW_I
            AIW_I = torch.mm(A, self.W_I)

            if self.featureless:
                if self.bias:
                    AIW_I = torch.add(AIW_I, self.b)

                return AIW_I

        FW_F = torch.mm(X, self.W_F)
        AFW_F = torch.mm(A, FW_F)

        AXW = torch.add(AIW_I, AFW_F) if self.input_layer else AFW_F

        if self.bias:
            AXW = torch.add(AXW, self.b)

        return AXW

    def init(self):
        # initialize weights from a uniform distribution following 
        # "Understanding the difficulty of training deep feedforward 
        #  neural networks" - Glorot, X. & Bengio, Y. (2010)
        for name, param in self.named_parameters():
            if name == 'b':
                # skip bias
                continue
            nn.init.xavier_uniform_(param)

        # initialize bias as null vector
        if self.bias:
            nn.init.zeros_(self.b)
