#!/usr/bin/python3

from collections import Counter
import logging
import gzip

from rdflib.graph import Graph
from rdflib.term import Literal
from rdflib.util import guess_format

from mrgcn.data.utils import is_gzip, is_readable


class RDFGraph:
    """ RDF Graph Class
    A wrapper around an imported rdflib.Graph object with convenience functions
    """
    graph = None
    _edge_distribution = {}

    def __init__(self, paths=None):
        self.logger = logging.getLogger(__name__)
        self.logger.debug("Initiating RDF Graph")

        if type(paths) is str:
            self.graph = self._read([paths])
        elif type(paths) is list:
            self.graph = self._read(paths)
        else:
            raise TypeError(":: Wrong input type: {}; requires path to RDF"
                            " graph".format(type(paths)))

        self._edge_distribution = Counter(self.graph.predicates())
        self.logger.debug("RDF Graph ({} facts) succesfully imported".format(len(self.graph)))

    def _read(self, paths=None):
        graph = Graph()
        for path in paths:
            assert is_readable(path)
            if not is_gzip(path):
                graph.parse(path, format=guess_format(path))
            else:
                self.logger.debug("Input recognized as gzip file")
                with gzip.open(path, 'rb') as f:
                    graph.parse(f, format=guess_format(path[:-3]))

        return graph

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.graph.destroy("store")
        self.graph.close(True)

    def __len__(self):
        return len(self.graph)

    ### Generators ###

    def nodes(self, separate_literals=True):
        self.logger.debug("Yielding nodes (separated literals: {})".format(
            separate_literals))
        seen = set()
        for s, p, o in self.graph.triples((None, None, None)):
            for node in (s, o):
                if separate_literals and isinstance(node, Literal):
                    node = self.UniqueLiteral(s, p, node)
                if node in seen:
                    continue
                seen.add(node)

                yield node

    def edges(self, unique=False):
        self.logger.debug("Yielding edges (unique: {})".format(unique))
        collection = set(self.graph.predicates()) if unique\
                    else self.graph.predicates()
        for edge in collection:
            yield(edge)

    def triples(self, triple=(None, None, None), separate_literals=True):
        self.logger.debug("Yielding triples (triple {})".format(triple))
        for s,p,o in self.graph.triples(triple):
            if separate_literals and isinstance(o, Literal):
                o = self.UniqueLiteral(s, p, o)
            yield s, p, o

    ## Statistics
    def edge_frequency(self, label=None):
        if label is None:
            return self._edge_distribution
        elif label in self._edge_distribution:
            return self._edge_distribution[label]

    ## Operators
    def sample(self, strategy=None, **kwargs):
        """ Sample this graph using the given strategy
        returns a RDFGraph instance
        """
        if strategy is None:
            raise ValueError('Strategy cannot be left undefined')

        self.logger.debug("Sampling graph")
        return strategy.sample(self, **kwargs)

    class UniqueLiteral(Literal):
        # literal with unique hash, irrespective of content
        def __new__(cls, s, p, o):
            self = super().__new__(cls, str(o), o.language, o.datatype, normalize=None)
            self.s = str(s)
            self.p = str(p)

            return self

        def __hash__(self):
            base = self.s + self.p + str(self)
            for attr in [self.language, self.datatype]:
                if attr is not None:
                    base += str(attr)

            return hash(base)

if __name__ == "__main__":
    print("RDF Graph")
