#! /usr/bin.env python

import argparse

from gvae.models import gae, gvae


def train():
    pass

def test():
    pass

def main(data, model, hidden, lr, nepoch, pdropout, bias):
    # unpack data and prep

    num_nodes = -1
    input_dim = -1
    hidden_dim, output_dim = hidden
    params = (input_dim, hidden_dim, output_dim, num_nodes, pdropout, bias)
    model = gae.GAE(*params) if model == "gae" else gvae.GVAE(*params)

    # optimizer
    # train
    # test
    # output

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--nepoch", help="Number of epoch", default=50)
    parser.add_argument("--lr", help="Initial learning rate", default=1e-2)
    parser.add_argument("--pdropout", help="Dropout probability", default=0.0)
    parser.add_argument("--bias", help="Add bias to layers", action="store_true")
    parser.add_argument("--model", help="Model to train",
                        choices=['gae', 'gvae'])
    parser.add_argument("--hidden", help="Number of nodes in hidden layers",
                        nargs=2, type=int, default=[32, 16])
    parser.add_argument("-i", "--input", help="Prepared input file (tar)")

    args = parser.parse_args()

    main(args.data,
         args.model,
         args.hidden,
         args.lr,
         args.nepoch,
         args.pdropout,
         args.bias)
