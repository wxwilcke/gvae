#! /usr/bin.env python

import torch
import torch.nn as nn
import torch.nn.functional as F

from gvae.layers.gcn import GraphConvolutionLayer


class GAE(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim,
                 num_nodes, p_dropout=0.0, bias=False):

        super().__init__()

        self.num_nodes = num_nodes
        self.p_dropout = p_dropout
        self.layers = nn.ModuleList()

        self.layers.append(
            GraphConvolutionLayer(input_dim=input_dim,
                                 output_dim=hidden_dim,
                                 num_nodes=num_nodes,
                                 bias=bias,
                                 input_layer=True,
                                 featureless=True)
        )

        self.layers.append(
            GraphConvolutionLayer(input_dim=hidden_dim,
                                 output_dim=output_dim,
                                 num_nodes=num_nodes,
                                 bias=bias,
                                 input_layer=False,
                                 featureless=False)
        )

    def forward(self, X, A):
        # compute node embeddings
        for i, layer in enumerate(self.layers):
            X = layer(X, A)
            if i == 0:
                X = F.relu(X)

            if self.p_dropout > 0.0:
                ones = F.dropout(torch.ones(self.num_nodes),
                                 p=self.p_dropout)
                X = torch.mul(X.T, ones).T

        Z = X  # embeddings
        A_hat = self.reconstruct(Z)

        return (Z, A_hat)

    def reconstruct(self, Z):
       A_hat = torch.mm(Z, Z.transpose(0, 1))
       return torch.sigmoid(A_hat)

    def init(self):
        for layer in self.layers:
            layer.init()
