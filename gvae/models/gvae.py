#! /usr/bin.env python

import torch
import torch.nn as nn
import torch.nn.functional as F

from gvae.layers.gcn import GraphConvolutionLayer


class GVAE(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim,
                 num_nodes, p_dropout=0.0, bias=False):

        super().__init__()

        self.num_nodes = num_nodes
        self.output_dim = output_dim
        self.p_dropout = p_dropout

        self.hidden =\
            GraphConvolutionLayer(input_dim=input_dim,
                                 output_dim=hidden_dim,
                                 num_nodes=num_nodes,
                                 bias=bias,
                                 input_layer=True,
                                 featureless=True)

        self.gcn_mu =\
            GraphConvolutionLayer(input_dim=hidden_dim,
                                 output_dim=output_dim,
                                 num_nodes=num_nodes,
                                 bias=bias,
                                 input_layer=False,
                                 featureless=False)

        self.gcn_sigma =\
            GraphConvolutionLayer(input_dim=hidden_dim,
                                 output_dim=output_dim,
                                 num_nodes=num_nodes,
                                 bias=bias,
                                 input_layer=False,
                                 featureless=False)

    def forward(self, X, A):
        X = self.hidden(X, A)
        if self.p_dropout > 0.0:
            ones = F.dropout(torch.ones(self.num_nodes),
                             p=self.p_dropout)
            X = torch.mul(X.T, ones).T
        H = F.relu(X)

        Z_mu = self.gcn_mu(H, A)
        Z_sigma = torch.exp(self.gcn_sigma(H, A))
        if self.p_dropout > 0.0:
            for tensor in (Z_mu, Z_sigma):
                ones = F.dropout(torch.ones(self.num_nodes),
                                 p=self.p_dropout)
                tensor = torch.mul(tensor.T, ones).T

        norm_size = (self.num_nodes, self.output_dim)
        Z = Z_mu + Z_sigma * torch.normal(0, 1, size=norm_size)
        A_hat = self.reconstruct(Z)

        return (Z, A_hat)

    def reconstruct(self, Z):
       A_hat = torch.mm(Z, Z.transpose(0,1))
       return torch.sigmoid(A_hat)

    def init(self):
        for layer in self.layers:
            layer.init()
